const express = require('express')
const app = express()
const port = 3000


let store ={
    "about": [
      { "id": 1, "title": "Sample  API" }
    ],
    "g": [
      { "id": 1, "body": "No Dammage found", "submission": 1 }
    ],
    "b": { "id":1,"body": "Dammage Found","dammage":[{"area":1,"type":"dent", "severity":"minor"}] }
  }

app.get('/g', (req, res) =>{
    
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(store.g, null, 3))
})

app.get('/b', (req, res) =>{
    
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(store.b, null, 3))
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))