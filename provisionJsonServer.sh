#!/bin/bash


#update the system
#sudo apt-get update -y && sudo apt-get upgrade -y

#ensure curl is installed
if [ $(dpkg-query -W -f='${Status}' curl 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  apt-get install curl;
else 
    curl -V
fi


#ensure git is installed
if [ $(dpkg-query -W -f='${Status}' git 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  apt-get install git;
else 
    git --version
fi


#check if nvm is installed and install node
if [ $(dpkg-query -W -f='${Status}' nvm 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
  export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
  nvm install node

else 
    nvm --version
fi

#install node-json
npm install -g express

#must clone the repository to get the latest version of the file added here as a starting point
git clone https://gitlab.com/alexekta/gcloudpublic.git
cd gcloudpublic
npm install

node serv.js


#start the node-json server with the db file on port 3000
#json-server --watch db.json